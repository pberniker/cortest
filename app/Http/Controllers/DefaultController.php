<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

use App\Services\SoupService;

class DefaultController extends Controller
{
    public function index(Request $request)
    {
        try {
            $soups = $request->post('soups');
            if (empty($soups)) { throw new \Exception("Variable 'soups' is required",400); }
            $soupService = new SoupService();
            $word = env('WORD');
            if (empty($word)) { throw new \Exception("Env 'WORD' is required",500); }
            $res = $soupService->getCount($soups, $word);
            return $res;
        }
        catch(\Exception $ex) {
            $code = $ex->getCode() > 200? $ex->getCode() : 500;
            $res = ['message' => $ex->getMessage()];
            return response()->json($res, $code);
        }
    }
}
