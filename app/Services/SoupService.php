<?php

namespace App\Services;

class SoupService
{
    #region Public

    public function getCount($input, $word)
    {
        $res = [];

        $soups = $this->map($input);

        foreach ($soups as $soup) {
            $count = $this->getCountInSoup($soup, $word);
            $res[] = $count;
        }

        return $res;
    }

    #endregion

    #region Private

    private function getCountInSoup(array $soup, $word)
    {
        $horizontalWords = $this->getHorizontalWords($soup);
        $verticalWords = $this->getVerticalWords($soup);
        $diagonalWords = $this->getDiagonalWords($soup);
        $words = array_merge($horizontalWords, $verticalWords, $diagonalWords);

        $res = 0;

        foreach ($words as $w) {
            $count = substr_count($w, $word);
            $res += $count;
        }

        return $res;
    }

    private function separate($str)
    {
        $clean = trim($str);
        $arr = preg_split("/\r|\n/", $clean);
        $numbers = [];
        $letters = [];

        foreach ($arr as $item) {
            $trimed = trim($item);
            $replaced = str_ireplace(' ', '', $trimed);
            $isDouble = is_numeric($replaced);

            if ($isDouble) {
                $numbers[] = $trimed;
            }
            else {
                $letters[] = $trimed;
            }
        }

        if (count($numbers) == 0) { $this->throwException(); }
        if (count($letters) == 0) { $this->throwException(); }

        $res = ['numbers' => $numbers, 'letters' => $letters];

        return $res;
    }

    private function map($str)
    {
        try {
            $res = [];

            $arr = $this->separate($str);

            $numbers = $arr['numbers'];
            $letters = $arr['letters'];

            $r = 0;

            foreach ($numbers as $number) {
                $rowsAndCols = explode(' ', $number);
                $rowsCount = $rowsAndCols[0];
                $colsCount = $rowsAndCols[1];
                $rowsArr = [];

                for ($row = 0; $row < $rowsCount; $row++) {
                    $lettersRow = $letters[$r];
                    $colsArr = [];
                    for ($col = 0; $col < $colsCount; $col++) { $colsArr[] = substr($lettersRow, $col,1); }
                    $rowsArr[] = $colsArr;
                    $r ++;
                }

                $res[] = $rowsArr;
            }

            return $res;
        }
        catch(\Exception $ex) {
            $this->throwException();
        }
    }

    private function getHorizontalWords(array $arr)
    {
        $res = [];

        foreach ($arr as $row) {
            $word = implode($row);
            $res[] = $word;
            if (strlen($word) > 1) { $res[] = strrev($word); }
        }

        return $res;
    }

    private function getVerticalWords(array $arr)
    {
        $res = [];

        $rows = count($arr);
        $cols = count($arr[0]);

        for ($c = 0; $c < $cols; $c ++) {
            $word = '';
            for ($r = 0; $r < $rows; $r ++) { $word .= $arr[$r][$c]; }
            $res[] = $word;
            if (strlen($word) > 1) { $res[] = strrev($word); }
        }

        return $res;
    }

    private function getDiagonalWords(array $arr)
    {
        $words1 = $this->getDiagonalWordsFromLeftToRight($arr);
        $invertedArr = $this->invertArray($arr);
        $words2 = $this->getDiagonalWordsFromLeftToRight($invertedArr);
        $res = array_merge($words1, $words2);
        return $res;
    }

    private function getDiagonalWordsFromLeftToRight(array $arr)
    {
        $res = [];
        $rows = count($arr);
        $cols = count($arr[0]);

        for ($j = 0; $j < $rows; $j++) {
            $word = '';

            for ($row = $j; $row > -1; $row--) {
                $col = $j - $row;
                if ($col < $cols) { $word .= $arr[$row][$col]; }
            }

            if (!empty($word)) {
                $res[] = $word;
                if (strlen($word) > 1) { $res[] = strrev($word); }
            }
        }

        for ($j = 1; $j < $rows; $j++) {
            $word = '';

            for ($row = $rows - 1; $row >= $j; $row--) {
                $col = $j + $rows - 1 - $row;
                if ($col < $cols) { $word .= $arr[$row][$col]; }
            }

            if (!empty($word)) {
                $res[] = $word;
                if (strlen($word) > 1) { $res[] = strrev($word); }
            }
        }

        return $res;
    }

    private function invertArray(array $arr)
    {
        $res = [];
        $count = count($arr);
        for($j = $count - 1; $j >= 0; $j--) { $res[] = $arr[$j]; }
        return $res;
    }

    private function throwException()
    {
        throw new \Exception("Variable 'soups' has invalid format",400);
    }

    #endregion
}
