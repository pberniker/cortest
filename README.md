### ¿Para qué sirve este repositorio? ###

* Test de sopa de letras para la empresa Cor.

### Configuración ###

* Hacer clone del repositorio "https://pberniker@bitbucket.org/pberniker/cortest.git".
* Copiar "env.example" en "env".
* Ejecutar "composer install".
* Ejecutar "php artisan serve".
* Abrir en el explorador "http://127.0.0.1:8000/". 

* Ejemplos de inputs:

    * 3 3<br/>
      ABC<br/>
      DEF<br/>
      HIJ
      
    * 2 3<br/>
      XYZ<br/>
      DEF
      
	* 5 4<br/>
	  FGHJ<br/>
	  VBNM<br/>
	  QWER<br/>
	  ZXCV<br/>
	  UIOP

### Test ###

* Para correr los test ejecutar "php artisan test".

### Tecnología ###

* Back: Api REST con Laravel 7 y PHP 7.3.7.
* Front: Html5 + JQuery.

