<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RequestTest extends TestCase
{
    public function testSuccessfully()
    {
        $soups = '
            3 3
            OIE
            IIX
            EXE
            1 10
            EIOIEIOEIO
            5 5
            EAEAE
            AIIIA
            EIOIE
            AIIIA
            EAEAE
            7 2
            OX
            IO
            EX
            II
            OX
            IE
            EX
            1 1
            E
        ';

        $response = $this->post('/api', ['soups' => $soups]);
        $arr = json_decode($response->getContent(),true);
        $this->assertEquals([3, 4, 8, 3, 0], $arr);
    }

    public function testBad()
    {
        $response = $this->post('/api', ['soups' => '']);
        $response->assertStatus(400);
    }
}
