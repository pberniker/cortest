<?php

namespace Tests\Unit;

use App\Services\SoupService;

abstract class BaseTest extends \Tests\TestCase
{
    #region Protected

    protected function getSoup($op)
    {
        if ($op == 1) {
            return [
                ['O', 'I', 'E'],
                ['I', 'I', 'X'],
                ['E', 'X', 'E'],
            ];
        }

        if ($op == 2) {
            return [
                ['E', 'I', 'O', 'I', 'E', 'I', 'O', 'E', 'I', 'O']
            ];
        }

        if ($op == 3) {
            return [
                ['E', 'A', 'E', 'A', 'E'],
                ['A', 'I', 'I', 'I', 'A'],
                ['E', 'I', 'O', 'I', 'E'],
                ['A', 'I', 'I', 'I', 'A'],
                ['E', 'A', 'E', 'A', 'E']
            ];
        }

        if ($op == 4) {
            return [
                ['O', 'X'],
                ['I', 'O'],
                ['E', 'X'],
                ['I', 'I'],
                ['O', 'X'],
                ['I', 'E'],
                ['E', 'X']
            ];
        }

        if ($op == 5) {
            return [
                ['E']
            ];
        }
    }

    protected function getService()
    {
        return new SoupService();
    }

    protected function getWord()
    {
        return 'OIE';
    }

    protected function assert($expected, $actual)
    {
        $this->assertEquals($expected, $actual, "Expected '{$expected}', but received '{$actual}'");
    }

    protected function getHorizontalWords(array $arr)
    {
        $res = [];

        foreach ($arr as $row) {
            $word = implode($row);
            $res[] = $word;
            if (strlen($word) > 1) { $res[] = strrev($word); }
        }

        return $res;
    }

    protected function getVerticalWords(array $arr)
    {
        $res = [];

        $rows = count($arr);
        $cols = count($arr[0]);

        for ($c = 0; $c < $cols; $c ++) {
            $word = '';
            for ($r = 0; $r < $rows; $r ++) { $word .= $arr[$r][$c]; }
            $res[] = $word;
            if (strlen($word) > 1) { $res[] = strrev($word); }
        }

        return $res;
    }

    protected function getDiagonalWords(array $arr)
    {
        $words1 = $this->getDiagonalWordsFromLeftToRight($arr);
        $invertedArr = $this->invertArray($arr);
        $words2 = $this->getDiagonalWordsFromLeftToRight($invertedArr);
        $res = array_merge($words1, $words2);
        return $res;
    }

    protected function getDiagonalWordsFromLeftToRight(array $arr)
    {
        $res = [];
        $rows = count($arr);
        $cols = count($arr[0]);

        for ($j = 0; $j < $rows; $j++) {
            $word = '';

            for ($row = $j; $row > -1; $row--) {
                $col = $j - $row;
                if ($col < $cols) { $word .= $arr[$row][$col]; }
            }

            if (!empty($word)) {
                $res[] = $word;
                if (strlen($word) > 1) { $res[] = strrev($word); }
            }
        }

        for ($j = 1; $j < $rows; $j++) {
            $word = '';

            for ($row = $rows - 1; $row >= $j; $row--) {
                $col = $j + $rows - 1 - $row;
                if ($col < $cols) { $word .= $arr[$row][$col]; }
            }

            if (!empty($word)) {
                $res[] = $word;
                if (strlen($word) > 1) { $res[] = strrev($word); }
            }
        }

        return $res;
    }

    protected function invertArray(array $arr)
    {
        $res = [];
        $count = count($arr);
        for($j = $count - 1; $j >= 0; $j--) { $res[] = $arr[$j]; }
        return $res;
    }

    protected function getCountInArray(array $soup, $word)
    {
        $horizontalWords = $this->getHorizontalWords($soup);
        $verticalWords = $this->getVerticalWords($soup);
        $diagonalWords = $this->getDiagonalWords($soup);
        $words = array_merge($horizontalWords, $verticalWords, $diagonalWords);

        $res = 0;

        foreach ($words as $w) {
            $count = substr_count($w, $word);
            $res += $count;
        }

        return $res;
    }

    #endregion
}
