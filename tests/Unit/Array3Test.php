<?php

namespace Tests\Unit;

class Array3Test extends BaseTest
{
    #region Members
    private $soup;
    #endregion

    #region Setup
    protected function setUp() : void
    {
        $this->soup = $this->getSoup(3);
    }
    #endregion

    #region Tests

    public function testArrayToHorizontalWords3()
    {
        // Act

        $words = $this->getHorizontalWords($this->soup);

        // Assert

        $this->assert(10, count($words));

        $this->assert('EAEAE', $words[0]);
        $this->assert('EAEAE', $words[1]);
    }

    public function testCount3()
    {
        // Act
        $res = $this->getCountInArray($this->soup, $this->getWord());

        // Assert
        $this->assert(8, $res);
    }

    #endregion
}
