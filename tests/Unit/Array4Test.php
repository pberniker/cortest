<?php

namespace Tests\Unit;

class Array4Test extends BaseTest
{
    #region Members
    private $soup;
    #endregion

    #region Setup
    protected function setUp() : void
    {
        $this->soup = $this->getSoup(4);
    }
    #endregion

    #region Tests

    public function testArrayToHorizontalWords5()
    {
        // Act

        $words = $this->getHorizontalWords($this->soup);

        // Assert

        $this->assert(14, count($words));

        $this->assert('OX', $words[0]);
        $this->assert('XO', $words[1]);

        $this->assert('IO', $words[2]);
        $this->assert('OI', $words[3]);

        $this->assert('EX', $words[4]);
        $this->assert('XE', $words[5]);
    }

    public function testArrayToVerticalWords5()
    {
        // Act

        $words = $this->getVerticalWords($this->soup);

        // Assert

        $this->assert(4, count($words));

        $this->assert('OIEIOIE', $words[0]);
        $this->assert('EIOIEIO', $words[1]);
    }

    public function testArrayToDiagonalWordsFromLeftToRight5()
    {
        // Act

        $res = $this->getDiagonalWordsFromLeftToRight($this->soup);

        // Assert

        $this->assert(14, count($res));

        $this->assert('O', $res[0]);

        $this->assert('IX', $res[1]);
        $this->assert('XI', $res[2]);

        $this->assert('EO', $res[3]);
        $this->assert('OE', $res[4]);

        $this->assert('IX', $res[5]);
        $this->assert('XI', $res[6]);

        $this->assert('OI', $res[7]);
        $this->assert('IO', $res[8]);

        $this->assert('IX', $res[9]);
        $this->assert('XI', $res[10]);

        $this->assert('EE', $res[11]);
        $this->assert('EE', $res[12]);

        $this->assert('X', $res[13]);
    }

    public function testCount4()
    {
        // Act
        $res = $this->getCountInArray($this->soup, $this->getWord());

        // Assert
        $this->assert(3, $res);
    }

    #endregion
}
