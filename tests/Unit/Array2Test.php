<?php

namespace Tests\Unit;

class Array2Test extends BaseTest
{
    #region Members
    private $soup;
    #endregion

    #region Setup
    protected function setUp() : void
    {
        $this->soup = $this->getSoup(2);
    }
    #endregion

    #region Tests

    public function testArrayToHorizontalWords2()
    {
        // Act

        $words = $this->getHorizontalWords($this->soup);

        // Assert

        $this->assert(2, count($words));

        $this->assert('EIOIEIOEIO', $words[0]);
    }

    public function testCount2()
    {
        // Act
        $res = $this->getCountInArray($this->soup, $this->getWord());

        // Assert
        $this->assert(4, $res);
    }

    #endregion
}
