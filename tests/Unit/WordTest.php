<?php

namespace Tests\Unit;

class WordTest extends BaseTest
{
    #region Members
    private $word;
    private $text;
    #endregion

    #region Setup
    protected function setUp() : void
    {
        $this->word = 'PaBlO';
        $this->text = "ABC{$this->word}efg{$this->word}HIJ{$this->word}";
    }
    #endregion

    #region Tests

    public function testCountWordsInString()
    {
        // Act
        $res = substr_count($this->text, $this->word);

        // Assert
        $this->assert(3, $res);
    }

    public function testInvertWord()
    {
        // Act
        $res = strrev($this->word);

        // Assert
        $this->assert('OlBaP', $res);
    }

    #endregion
}
