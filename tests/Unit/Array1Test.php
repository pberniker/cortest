<?php

namespace Tests\Unit;

class Array1Test extends BaseTest
{
    #region Members
    private $soup;
    #endregion

    #region Setup
    protected function setUp() : void
    {
        $this->soup = $this->getSoup(1);
    }
    #endregion

    #region Tests

    public function testArrayToHorizontalWords1()
    {
        // Act

        $words = $this->getHorizontalWords($this->soup);

        // Assert

        $this->assert(6, count($words));

        $this->assert('OIE', $words[0]);
        $this->assert('EIO', $words[1]);

        $this->assert('IIX', $words[2]);
        $this->assert('XII', $words[3]);
    }

    public function testArrayToVerticalWords1()
    {
        // Act

        $words = $this->getVerticalWords($this->soup);

        // Assert

        $this->assert(6, count($words));

        $this->assert('OIE', $words[0]);
        $this->assert('EIO', $words[1]);

        $this->assert('IIX', $words[2]);
        $this->assert('XII', $words[3]);
    }

    public function testArrayToDiagonalWordsFromLeftToRight1()
    {
        // Act

        $res = $this->getDiagonalWordsFromLeftToRight($this->soup);

        // Assert

        $this->assert(8, count($res));

        $this->assert('O', $res[0]);

        $this->assert('II', $res[1]);
        $this->assert('II', $res[2]);

        $this->assert('EIE', $res[3]);
        $this->assert('EIE', $res[4]);

        $this->assert('XX', $res[5]);
        $this->assert('XX', $res[6]);

        $this->assert('E', $res[7]);
    }

    public function testInvertArray1()
    {
        #Inverse
        /*
        ['E', 'X', 'e'],
        ['I', 'I', 'x'],
        ['O', 'i', 'E'],
        */

        // Act

        $res = $this->invertArray($this->soup);

        // Assert

        $this->assert(3, count($res));

        $this->assert('E', $res[0][0]);
        $this->assert('X', $res[0][1]);
        $this->assert('E', $res[0][2]);

        $this->assert('I', $res[1][0]);
        $this->assert('I', $res[1][1]);
        $this->assert('X', $res[1][2]);

        $this->assert('O', $res[2][0]);
        $this->assert('I', $res[2][1]);
        $this->assert('E', $res[2][2]);
    }

    public function testArrayInverseToDiagonalWords1()
    {
        // Act

        $res = $this->getDiagonalWordsFromLeftToRight($this->invertArray($this->soup));

        // Assert

        $this->assert(8, count($res));

        $this->assert('E', $res[0]);

        $this->assert('IX', $res[1]);
        $this->assert('XI', $res[2]);

        $this->assert('OIE', $res[3]);
        $this->assert('EIO', $res[4]);

        $this->assert('IX', $res[5]);
        $this->assert('XI', $res[6]);

        $this->assert('E', $res[7]);
    }

    public function testArrayToDiagonalWords1()
    {
        // Act

        $res = $this->getDiagonalWords($this->soup);

        // Assert

        $this->assert(16, count($res));


        $this->assert('O', $res[0]);

        $this->assert('II', $res[1]);
        $this->assert('II', $res[2]);

        $this->assert('EIE', $res[3]);
        $this->assert('EIE', $res[4]);

        $this->assert('XX', $res[5]);
        $this->assert('XX', $res[6]);

        $this->assert('E', $res[7]);

        $this->assert('E', $res[8]);


        $this->assert('IX', $res[9]);
        $this->assert('XI', $res[10]);

        $this->assert('OIE', $res[11]);
        $this->assert('EIO', $res[12]);

        $this->assert('IX', $res[13]);
        $this->assert('XI', $res[14]);

        $this->assert('E', $res[15]);
    }

    public function testCount1()
    {
        // Act
        $res = $this->getCountInArray($this->soup, $this->getWord());

        // Assert
        $this->assert(3, $res);
    }

    #endregion
}
