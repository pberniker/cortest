<?php

namespace Tests\Unit;

class SoupServiceTest extends BaseTest
{
    #region SetUp
    protected function setUp() : void
    {
        $this->soupService = $this->getService();
        $this->word = 'OIE';

        $this->input = '
            3 3
            OIE
            IIX
            EXE
            1 10
            EIOIEIOEIO
            5 5
            EAEAE
            AIIIA
            EIOIE
            AIIIA
            EAEAE
            7 2
            OX
            IO
            EX
            II
            OX
            IE
            EX
            1 1
            E
        ';
    }
    #endregion

    #region Tests

    public function testSeparate()
    {
        // Act

        $res = $this->separate($this->input);

        $numbers = $res['numbers'];
        $letters = $res['letters'];

        // Assert

        $this->assert('3 3', $numbers[0]);
        $this->assert('1 10', $numbers[1]);

        $this->assert('OIE', $letters[0]);
        $this->assert('IIX', $letters[1]);
        $this->assert('EXE', $letters[2]);

        $this->assert('EIOIEIOEIO', $letters[3]);
    }

    public function testMap()
    {
        // Act

        $res = $this->map($this->input);

        // Assert

        $this->assert(5, count($res));

        $this->assert('OIE', implode('', $res[0][0]));
        $this->assert('IIX', implode('', $res[0][1]));
        $this->assert('EXE', implode('', $res[0][2]));

        $this->assert('EIOIEIOEIO', implode('', $res[1][0]));

        $this->assert('EAEAE', implode('', $res[2][0]));
        $this->assert('AIIIA', implode('', $res[2][1]));
    }

    public function testCount()
    {
        // Act

        $res = $this->soupService->getCount($this->input, $this->word);

        // Assert

        $this->assert(3, $res[0]);
        $this->assert(4, $res[1]);
        $this->assert(8, $res[2]);
        $this->assert(3, $res[3]);
        $this->assert(0, $res[4]);
    }

    #endregion

    #region Private

    private function separate($str)
    {
        $clean = trim($str);
        $arr = preg_split("/\r|\n/", $clean);
        $numbers = [];
        $letters = [];

        foreach ($arr as $item) {
            $trimed = trim($item);
            $replaced = str_ireplace(' ', '', $trimed);
            $isDouble = is_numeric($replaced);

            if ($isDouble) {
                $numbers[] = $trimed;
            }
            else {
                $letters[] = $trimed;
            }
        }

        $res = ['numbers' => $numbers, 'letters' => $letters];

        return $res;
    }

    private function map($str)
    {
        $res = [];

        $arr = $this->separate($str);

        $numbers = $arr['numbers'];
        $letters = $arr['letters'];

        $r = 0;

        foreach ($numbers as $number) {
            $rowsAndCols = explode(' ', $number);
            $rowsCount = $rowsAndCols[0];
            $colsCount = $rowsAndCols[1];
            $rowsArr = [];

            for ($row = 0; $row < $rowsCount; $row++) {
                $lettersRow = $letters[$r];
                $colsArr = [];
                for ($col = 0; $col < $colsCount; $col++) { $colsArr[] = substr($lettersRow, $col,1); }
                $rowsArr[] = $colsArr;
                $r ++;
            }

            $res[] = $rowsArr;
        }

        return $res;
    }

    #endregion
}
