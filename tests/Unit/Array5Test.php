<?php

namespace Tests\Unit;

class Array5Test extends BaseTest
{
    #region Members
    private $soup;
    #endregion

    #region Setup
    protected function setUp() : void
    {
        $this->soup = $this->getSoup(5);
    }
    #endregion

    #region Tests

    public function testArrayToHorizontalWords5()
    {
        // Act

        $words = $this->getHorizontalWords($this->soup);

        // Assert

        $this->assert(1, count($words));

        $this->assert('E', $words[0]);
    }

    public function testCount5()
    {
        // Act
        $res = $this->getCountInArray($this->soup, $this->getWord());

        // Assert
        $this->assert(0, $res);
    }

    #endregion
}
