$(function() {
    $('#send').click(function() {
		send();
	});
});

function send() {
	$('#loader').show();
    $('#error').hide();
	$('#result').hide();
	openDialog();

    var options = {
        url: '/api',
        type: 'post',
		data: { soups: $('#soups').val() },
        cache: false,
        success: success,
        error: error
    };

    console.clear();
    console.log('Call ajax ...');
    console.log('options:', options);
    console.log();

    $.ajax(options);
}

function success(res) {
	console.log('res:', res);
	var html = res.length == 1? res[0] : res.join(', ');
	$('#result').html(html);
	$('#result').show();
	$('#loader').hide();
}

function error(jaXHR, status, error) {
	var statusCode = jaXHR.status;

	console.log('jaXHR:', jaXHR);
	console.log('statusCode:', statusCode);
	console.log('status:', status);

	var res = null;
	if (jaXHR.responseJSON) { res = jaXHR.responseJSON; }
	else if (jaXHR.responseText) { res = JSON.parse(jaXHR.responseText); }

	console.log('res:', res);

	var message = res? res.message : 'Unknown error';

	$('#error').html(message);
	$('#error').show();
	$('#loader').hide();
}

function openDialog() {
	var options = {
		resizable: false,
		height: 'auto',
		width: '350',
		modal: true
	};

	$('#dialog').dialog(options);
	$('#dialog').dialog('open');
}
