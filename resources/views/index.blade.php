<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <title>Cor</title>
    </head>
    <body>
        <div class="container">
            <h1 style="margin-top: 20px; margin-bottom: 20px;">Letters soup test</h1>
            <form>
                <div class="form-group">
                            <textarea
                                class="form-control"
                                id="soups"
                                rows="20"
                                cols="50"
                                placeholder=
                                "3 3
        OIE
        IIX
        EXE

        1 10
        EIOIEIOEIO

        5 5
        EAEAE
        AIIIA
        EIOIE
        AIIIA
        EAEAE

        7 2
        OX
        IO
        EX
        II
        OX
        IE
        EX

        1 1
        E"
                            ></textarea>
                </div>
                <button type="button" id="send" class="btn btn-primary">Send</button>
            </form>
        </div>
        <div id="dialog" style="display: none;" title="">
            <img src="/images/ajax-loader.gif" id="loader" style="display: none; margin-left: 10px;" />
            <div id="error" class="alert alert-danger" role="alert" style="display: none;"></div>
            <div id="result" class="alert alert-success" role="alert" style="display: none;"></div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="/js/main.js"></script>
    </body>
</html>
